*** branch new_version: Currenlty developing a new version of toko: which is a struct redesign and naming standardisation, this will be a new base of toko, thank you for the feedback Mr. Evan from sale stock ***

# Toko

Toko is an Go Rest API assessment task for sale stock recruitment

# Usage
Linux binary & Windows library (Compiled in Linux Ubuntu & Windows 10) is precompiled inside build/linux directory

Make sure you have right premission

**./toko** to run

**Port is 1234**

#Library
- "bytes"
- "database/sql"
- "encoding/csv"
- "encoding/json"
- "fmt"
- "net/http"
- "os"
- "strconv"
- "strings"
- "time"	
- "github.com/gorilla/mux"
- "github.com/mattn/go-sqlite3"
- "github.com/sirupsen/logrus"

# Documentation

**I did not list all API end point here. Take a look at main.go for complete API endpoints.**

## Item
### Add item (post)
*/addItem*

**status:** 
- 1 : active
- 2 : removed

**PostBody**
```json
{
	"sku" : "SSI-D00791015-LL-BWH",
	"name" : "Zalekia Plain Casual Blouse",
	"colour" : "Broken White",
	"size" : "L",
	"status": 1
}
```
**Response : 200 OK**

### Edit item (post)
*/editItem/{item_id}*

***PostBody***
```json
{
	"sku" : "SSI-D00791015-LL-BWH",
	"name" : "Zalekia Plain Casual Blouse",
	"colour" : "Broken White",
	"size" : "L",
	"status": 1
}
```
**Response : 200 OK**

### Remove item (get)
*/removeItem/{item_id}*
**Response : 200 OK**

### Get all items (get)
*/getItems*
***Response:***
```json
[
	{
		"item_id": 6,
		"sku": "SSI-D00791015-LL-BWH",
		"name": "Zalekia Plain Casual Blouse",
		"colour": "Broken White",
		"size": "XL",
		"status": 1
	}
]
```

### Get Item By ID (get)
*/getItem/{itemID}*
***Response:***
```json
{
	"item_id": 6,
	"sku": "SSI-D00791015-LL-BWH",
	"name": "Zalekia Plain Casual Blouse",
	"colour": "Broken White",
	"size": "XL",
	"status": 1
}
```

### Get Item By SKU (get)
*/getItemBySKU/{sku}*

## Kwitansi
### Add kwitansi (post)
*/addKwitansi*

**status:** 
- 1 : active
- 2 : removed

**PostBody**
```json
{
	"no" : "20170823-75140",
	"status": 1
}
```
**Response : 200 OK**

### Edit kwitansi (post)
*/editKwitansi/{kwitansiID}*

**PostBody**
```json
{
	"no" : "20170823-75140",
	"status": 1
}
```
**Response : 200 OK**

### Remove kwitansi (GET)
*/removeKwitansi/{kwitansiID}*
**Response : 200 OK**

### Get all kwitansi (GET)
*/getAllKwitansi*

### Get kwitansi by ID (GET)
*/getKwitansi/{kwitansiID}*

### Get detail kwitansi by kwitansi number (GET)
*/getKwitansiDetail/{no}*

**Response : 200 OK**
**Response body:**
```json
{
    "kwitansi": {
        "kwitansi_id": 5,
        "no": "20170823-75140",
        "status": 1
    },
    "items": [
        {
            "item": {
                "item_id": 6,
                "sku": "SSI-D00791015-LL-BWH",
                "name": "Zalekia Plain Casual Blouse",
                "colour": "Broken White",
                "size": "XL",
                "status": 1
            },
            "pemesanan": {
                "barang_masuk_id": 6,
                "jmlh_pemesanan": 41,
                "harga_beli": 60000,
                "status": 1,
                "total": 2460000
            },
            "detil": [
                {
                    "detil_id": 6,
                    "date": "2012-09-25T00:00:00Z",
                    "jmlh_diterima": 20
                },
                {
                    "detil_id": 7,
                    "date": "2012-09-30T00:00:00Z",
                    "jmlh_diterima": 21
                }
            ]
        }
    ]
}
```

## Barang Masuk
### Add barang masuk (POST)
*/addBarangMasuk*

**status:** 
- 1 : active
- 2 : removed

**PostBody**
```json
{
	"jmlh_pemesanan" : 41,
	"harga_beli": 60000,
	"status" : 1,
	"item" : { 
		"item_id" : 6
	},
	"kwitansi" : {
		"kwitansi_id" : 5
	}
}
```
** Response: 200 OK**
** Response body**
```json
{
    "barang_masuk_id": 6
}
```

## Detil barang masuk
### Add detil barang masuk (post)
*/addDetilBarangMasuk/{barangMasukID}*

**Post body**
```json
{
	"jmlh_diterima": 20,
	"date" : "2012-09-25"
}
```
**Response : 200 OK**

## Barang Keluar
### Add barang keluar (post)
*/addBarangKeluar*

**Post Body**
```json
{
	"date_time": "2012-11-30 23:15:00",
	"notes" : "Pesanan tiba dengan baik",
	"order_id" : "ID-20130102-901847",
	"detil" : [
		{
			"item" : {
				"item_id" : 6
			},
			"jmlh_keluar" : 20,
			"harga_jual" : 80000
		}
	]
}
```
**Response: 200 OK**
**Response Body**
```json
{
    "barang_keluar_id": 12
}
```

### Get all barang keluar (get)
*/getAllBarangKeluar*

**Response Body**
```json
[
    {
        "barang_keluar_id": 12,
        "order_id": "ID-20130102-901847",
        "date_time": "2012-11-30T23:15:00Z",
        "notes": "Pesanan tiba dengan baik",
        "detil": [
            {
                "item": {
                    "item_id": 6,
                    "sku": "SSI-D00791015-LL-BWH",
                    "name": "Zalekia Plain Casual Blouse",
                    "colour": "Broken White",
                    "size": "XL",
                    "status": 1
                },
                "jmlh_keluar": 20,
                "harga_jual": 80000
            }
        ]
    }
]
```

## Laporan Nilai Barang (Get)

### Get Laporan Nilai Barang (Get)
*/laporanNilaiBarang*

**Response Body**
```json
{
    "TanggalCetak": "01 February 2018",
    "JumlahSKU": 1,
    "JumlahTotalBarang": 21,
    "TotalNilai": 1260000,
    "Items": [
        {
            "Sku": "SSI-D00791015-LL-BWH",
            "Name": "Zalekia Plain Casual Blouse",
            "Size": "XL",
            "Colour": "Broken White",
            "BarangMasuk": 41,
            "BarangKeluar": 20,
            "AverageBuyPrice": 60000,
            "Stock": 21,
            "Total": 1260000
        }
    ]
}
```

### Export CSV Laporan Nilai Barang (Get)
*/laporanNilaiBarang/export*

## Laporan Penjualan

### Get Laporan Penjualan (Get)
*/laporanPenjualan/{startDate}/{endDate}*

**Example**
localhost:1234/laporanPenjualan/2010-02-03/2012-12-03

**Notes:**
- Laba kotor adalah total laba yang memiliki id pemesanan
- Total penjualan adalah count dari penjualan yang memili id pemesanan

**Response Body**
```json
{
    "TanggalCetak": "01 February 2018",
    "Tanggal": "2010-02-03 - 2012-12-03",
    "TotalOmzet": 1600000,
    "TotalLabaKotor": 400000,
    "TotalPenjualan": 1,
    "TotalBarang": 20,
    "Items": [
        {
            "OrderID": "ID-20130102-901847",
            "Date": "2012-11-30T23:15:00Z",
            "SKU": "SSI-D00791015-LL-BWH",
            "Name": "Zalekia Plain Casual Blouse",
            "Size": "XL",
            "Colour": "Broken White",
            "Jumlah": 20,
            "HargaJual": 80000,
            "Total": 1600000,
            "HargaBeli": 60000,
            "Laba": 400000
        }
    ]
}
```

### Export CSV Laporan Penjualan (Get)
*/laporanPenjualan/export/{startDate}/{endDate}*

**Example**
localhost:1234/laporanPenjualan/export/2010-02-03/2012-12-03