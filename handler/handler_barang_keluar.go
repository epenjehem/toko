package handler

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/toko/logger"
	"bitbucket.org/toko/model"
	"github.com/gorilla/mux"
)

// GetAllBarangKeluar Handler: get all item that user sell
func GetAllBarangKeluar(w http.ResponseWriter, req *http.Request) {
	barang, err := model.GetAllBarangKeluar()

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	output, err := json.MarshalIndent(&barang, "", "\t\t")

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(output)
	return
}

// AddBarangKeluar Handler: create new item that user sell
func AddBarangKeluar(w http.ResponseWriter, req *http.Request) {

	var barangKeluar model.BarangKeluar

	err := json.NewDecoder(req.Body).Decode(&barangKeluar)

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	barangKeluarID, err := model.AddBarangKeluar(barangKeluar)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var r struct {
		BarangKeluarID int `json:"barang_keluar_id"`
	}

	r.BarangKeluarID = barangKeluarID

	output, err := json.MarshalIndent(&r, "", "\t\t")

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(output)

	return
}

// EditBarangKeluar Handler: update item that user sell
func EditBarangKeluar(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	barangKeluarID, err := strconv.Atoi(vars["barangKeluarID"])
	logger.Log(err, logger.ERROR)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var barangKeluar model.BarangKeluar

	err = json.NewDecoder(req.Body).Decode(&barangKeluar)

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = model.EditBarangKeluar(barangKeluarID, barangKeluar)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return

}
