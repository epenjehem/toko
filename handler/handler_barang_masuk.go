package handler

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/toko/logger"
	"bitbucket.org/toko/model"
	"github.com/gorilla/mux"
)

// GetBarangMasuk Handler: retrieve item that user buy based on given ID
func GetBarangMasuk(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	barangMasukID, err := strconv.Atoi(vars["barangMasukID"])
	logger.Log(err, logger.ERROR)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	barang, err := model.GetBarangMasuk(barangMasukID)

	if err == sql.ErrNoRows {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	output, err := json.MarshalIndent(&barang, "", "\t\t")

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(output)
	return
}

// AddBarangMasuk Handler: create item that user buy
func AddBarangMasuk(w http.ResponseWriter, req *http.Request) {

	var barangMasuk model.BarangMasuk

	err := json.NewDecoder(req.Body).Decode(&barangMasuk)

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	barangID, err := model.AddBarangMasuk(barangMasuk)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var r struct {
		BarangMasukID int `json:"barang_masuk_id"`
	}

	r.BarangMasukID = barangID

	output, err := json.MarshalIndent(&r, "", "\t\t")

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(output)

	return
}

// EditBarangMasuk Handler: update item that user buy based on given ID
func EditBarangMasuk(w http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	barangMasukID, err := strconv.Atoi(vars["barangMasukID"])
	logger.Log(err, logger.ERROR)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var barangMasuk model.BarangMasuk

	err = json.NewDecoder(req.Body).Decode(&barangMasuk)

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = model.EditBarangMasuk(barangMasukID, barangMasuk)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

// AddDetilBarangMasuk Handler: create detil of item that user buy based on given ID
func AddDetilBarangMasuk(w http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	barangMasukID, err := strconv.Atoi(vars["barangMasukID"])
	logger.Log(err, logger.ERROR)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var detil model.DetilBarangMasuk

	err = json.NewDecoder(req.Body).Decode(&detil)

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = model.AddDetilBarangMasuk(barangMasukID, detil)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

// EditDetilBarangMasuk Handler: update detil of item that user buy based on given detail ID
func EditDetilBarangMasuk(w http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	detilID, err := strconv.Atoi(vars["detilID"])
	logger.Log(err, logger.ERROR)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var detil model.DetilBarangMasuk

	err = json.NewDecoder(req.Body).Decode(&detil)

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = model.EditDetilBarangMasuk(detilID, detil)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

// RemoveDetilBarangMasuk Handler: delete detil of item that user buy based on given detail ID
func RemoveDetilBarangMasuk(w http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	detilID, err := strconv.Atoi(vars["detilID"])
	logger.Log(err, logger.ERROR)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = model.DeleteDetilBarangMasuk(detilID)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}
