package handler

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/toko/logger"
	"bitbucket.org/toko/model"
	"github.com/gorilla/mux"
)

// GetItems Get all items
func GetItems(w http.ResponseWriter, req *http.Request) {
	items, err := model.GetItems()

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	output, err := json.MarshalIndent(&items, "", "\t\t")

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(output)
	return

}

// GetItem Get item by ID
func GetItem(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	itemID, err := strconv.Atoi(vars["itemID"])
	logger.Log(err, logger.ERROR)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	item, err := model.GetItem(itemID)

	if err == sql.ErrNoRows {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	output, err := json.MarshalIndent(&item, "", "\t\t")

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(output)
	return
}

// GetItemBySKU get item by SKU
func GetItemBySKU(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	sku := vars["sku"]
	item, err := model.GetItemBySKU(sku)

	if err == sql.ErrNoRows {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	output, err := json.MarshalIndent(&item, "", "\t\t")

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(output)
	return
}

// AddItem create new item
func AddItem(w http.ResponseWriter, req *http.Request) {

	var item model.Item

	err := json.NewDecoder(req.Body).Decode(&item)

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = model.AddItem(item)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

// EditItem update item
func EditItem(w http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	itemID, err := strconv.Atoi(vars["itemID"])
	logger.Log(err, logger.ERROR)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var item model.Item

	err = json.NewDecoder(req.Body).Decode(&item)

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = model.EditItem(itemID, item)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

// DeleteItem remove item soft delete
func DeleteItem(w http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	itemID, err := strconv.Atoi(vars["itemID"])
	logger.Log(err, logger.ERROR)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = model.DeleteItem(itemID)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}
