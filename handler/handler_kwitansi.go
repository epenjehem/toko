package handler

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/toko/logger"
	"bitbucket.org/toko/model"
	"github.com/gorilla/mux"
)

func GetAllKwitansi(w http.ResponseWriter, req *http.Request) {
	kws, err := model.GetAllKwitansi()

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	output, err := json.MarshalIndent(&kws, "", "\t\t")

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(output)
	return

}

func GetKwitansi(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	kwitansiID, err := strconv.Atoi(vars["kwitansiID"])
	logger.Log(err, logger.ERROR)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	kwitansi, err := model.GetKwitansi(kwitansiID)

	if err == sql.ErrNoRows {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	output, err := json.MarshalIndent(&kwitansi, "", "\t\t")

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(output)
	return
}

func GetKwitansiDetail(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	no := vars["no"]

	kwitansi, err := model.GetKwitansiDetail(no)

	if err == sql.ErrNoRows {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	output, err := json.MarshalIndent(&kwitansi, "", "\t\t")

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(output)
	return
}

func AddKwitansi(w http.ResponseWriter, req *http.Request) {

	var kwitansi model.KwitansiBarangMasuk

	err := json.NewDecoder(req.Body).Decode(&kwitansi)

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = model.AddKwitansi(kwitansi)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

func EditKwitansi(w http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	kwitansiID, err := strconv.Atoi(vars["kwitansiID"])
	logger.Log(err, logger.ERROR)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var kwitansi model.KwitansiBarangMasuk

	err = json.NewDecoder(req.Body).Decode(&kwitansi)

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = model.EditKwitansi(kwitansiID, kwitansi)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

func DeleteKwitansi(w http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	kwitansiID, err := strconv.Atoi(vars["kwitansiID"])
	logger.Log(err, logger.ERROR)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = model.DeleteKwitansi(kwitansiID)
	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}
