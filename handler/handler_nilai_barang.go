package handler

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/toko/logger"
	"bitbucket.org/toko/model"
	"github.com/gorilla/mux"
)

func LaporanNilaiBarang(w http.ResponseWriter, req *http.Request) {
	laporan, err := model.LaporanNilaiBarang()

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	output, err := json.MarshalIndent(&laporan, "", "\t\t")

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(output)
	return
}

func ExportLaporanNilaiBarang(w http.ResponseWriter, req *http.Request) {
	laporan, err := model.LaporanNilaiBarang()

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	b := &bytes.Buffer{}
	write := csv.NewWriter(b)

	if err := write.Write([]string{"SKU", "Nama Item", "Size", "Colour", "Stock", "Rata-Rata Harga Beli", "Total"}); err != nil {
		logger.Log(err, logger.ERROR)
	}

	for _, item := range laporan.Items {
		var record []string
		record = append(record, item.Sku)
		record = append(record, item.Name)
		record = append(record, item.Size)
		record = append(record, item.Colour)
		record = append(record, strconv.Itoa(item.Stock))
		record = append(record, strconv.FormatFloat(item.AverageBuyPrice, 'f', -1, 64))
		record = append(record, strconv.FormatFloat(item.Total, 'f', -1, 64))

		if err := write.Write(record); err != nil {
			logger.Log(err, logger.ERROR)
		}
	}

	write.Flush()

	if err := write.Error(); err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Description", "File Transfer")
	w.Header().Set("Content-Disposition", "attachment; filename=laporan-nilai-barang-"+strings.Replace(laporan.TanggalCetak, " ", "-", 2)+".csv")
	w.Header().Set("Content-Type", "text/csv")
	w.WriteHeader(http.StatusOK)
	w.Write(b.Bytes())
}

func LaporanPenjualan(w http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	startDate := vars["startDate"]
	endDate := vars["endDate"]

	laporan, err := model.LaporanPenjualan(startDate, endDate)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	output, err := json.MarshalIndent(&laporan, "", "\t\t")

	if err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(output)
	return
}

func ExportLaporanPenjualan(w http.ResponseWriter, req *http.Request) {

	//localhost:8080/laporanPenjualan/export/2010-02-03/2012-12-03

	vars := mux.Vars(req)
	startDate := vars["startDate"]
	endDate := vars["endDate"]

	laporan, err := model.LaporanPenjualan(startDate, endDate)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	b := &bytes.Buffer{}
	write := csv.NewWriter(b)

	if err := write.Write([]string{"Pesanan", "Waktu", "SKU", "Nama Barang", "Jumlah", "Harga Jual", "Total", "Harga Beli", "Laba"}); err != nil {
		logger.Log(err, logger.ERROR)
	}

	for _, item := range laporan.Items {
		var record []string
		record = append(record, item.OrderID)
		record = append(record, item.Date)
		record = append(record, item.SKU)
		record = append(record, item.Name)
		record = append(record, strconv.Itoa(item.Jumlah))
		record = append(record, strconv.Itoa(item.HargaJual))
		record = append(record, strconv.Itoa(item.Total))
		record = append(record, strconv.Itoa(item.HargaBeli))
		record = append(record, strconv.Itoa(item.Laba))

		if err := write.Write(record); err != nil {
			logger.Log(err, logger.ERROR)
		}
	}

	write.Flush()

	if err := write.Error(); err != nil {
		logger.Log(err, logger.ERROR)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Description", "File Transfer")
	w.Header().Set("Content-Disposition", "attachment; filename=laporan-keuangan-"+strings.Replace(laporan.TanggalCetak, " ", "-", 2)+".csv")
	w.Header().Set("Content-Type", "text/csv")
	w.WriteHeader(http.StatusOK)
	w.Write(b.Bytes())

	return
}
