package logger

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
)

const (
	// INFO : log level info
	INFO = "info"
	// WARNING : log level warning
	WARNING = "warning"
	// ERROR : log level error
	ERROR = "error"
	// FATAL : log level fatal
	FATAL = "fatal"
	// PANIC : log level panic
	PANIC = "panic"
)

// Log handle error log. Accpeting error and level (info, warning, error, fatal, panic)
func Log(e error, level string) {

	if e != nil {
		filename := "logfile.log"

		f, err := os.OpenFile(filename, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
		defer f.Close()

		formatter := new(log.TextFormatter)
		formatter.TimestampFormat = "02-01-2006 15:04:05"
		formatter.FullTimestamp = true

		log.SetFormatter(formatter)

		if err != nil {
			// can not open file, logging to stderr
			fmt.Println(err)
		} else {
			log.SetOutput(f)
		}

		switch level {
		case INFO:
			log.Info(e)
		case WARNING:
			log.Warning(e)
		case ERROR:
			log.Error(e)
		case FATAL:
			log.Fatal(e)
		case PANIC:
			log.Panic(e)
		default:
			log.Warning(e)
		}
	}

}
