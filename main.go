package main

import (
	"net/http"

	"bitbucket.org/toko/handler"
	"github.com/gorilla/mux"
)

func main() {

	router := mux.NewRouter().StrictSlash(false)
	router.HandleFunc("/getItems", handler.GetItems).Methods("GET")
	router.HandleFunc("/getItem/{itemID}", handler.GetItem).Methods("GET")
	router.HandleFunc("/getItemBySKU/{sku}", handler.GetItemBySKU).Methods("GET")
	router.HandleFunc("/addItem", handler.AddItem).Methods("POST")
	router.HandleFunc("/editItem/{itemID}", handler.EditItem).Methods("POST")
	router.HandleFunc("/removeItem/{itemID}", handler.DeleteItem).Methods("GET")

	router.HandleFunc("/getAllKwitansi", handler.GetAllKwitansi).Methods("GET")
	router.HandleFunc("/getKwitansi/{kwitansiID}", handler.GetKwitansi).Methods("GET")
	router.HandleFunc("/getKwitansiDetail/{no}", handler.GetKwitansiDetail).Methods("GET")
	router.HandleFunc("/addKwitansi", handler.AddKwitansi).Methods("POST")
	router.HandleFunc("/editKwitansi/{kwitansiID}", handler.EditKwitansi).Methods("POST")
	router.HandleFunc("/removeKwitansi/{kwitansiID}", handler.DeleteKwitansi).Methods("GET")

	router.HandleFunc("/getBarangMasuk/{barangMasukID}", handler.GetBarangMasuk).Methods("GET")
	router.HandleFunc("/addBarangMasuk", handler.AddBarangMasuk).Methods("POST")
	router.HandleFunc("/editBarangMasuk/{barangMasukID}", handler.EditBarangMasuk).Methods("POST")

	router.HandleFunc("/addDetilBarangMasuk/{barangMasukID}", handler.AddDetilBarangMasuk).Methods("POST")
	router.HandleFunc("/editDetilBarangMasuk/{detilID}", handler.EditDetilBarangMasuk).Methods("POST")
	router.HandleFunc("/removeDetilBarangMasuk/{detilID}", handler.RemoveDetilBarangMasuk).Methods("GET")

	router.HandleFunc("/getAllBarangKeluar", handler.GetAllBarangKeluar).Methods("GET")
	router.HandleFunc("/addBarangKeluar", handler.AddBarangKeluar).Methods("POST")
	router.HandleFunc("/editBarangKeluar/{barangKeluarID}", handler.EditBarangKeluar).Methods("POST")

	router.HandleFunc("/laporanNilaiBarang", handler.LaporanNilaiBarang).Methods("GET")
	router.HandleFunc("/laporanNilaiBarang/export", handler.ExportLaporanNilaiBarang).Methods("GET")
	router.HandleFunc("/laporanPenjualan/{startDate}/{endDate}", handler.LaporanPenjualan).Methods("GET")
	router.HandleFunc("/laporanPenjualan/export/{startDate}/{endDate}", handler.ExportLaporanPenjualan).Methods("GET")

	server := http.Server{
		Addr:    ":1234",
		Handler: router,
	}

	server.ListenAndServe()

}
