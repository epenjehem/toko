package model

// Item barang jualan
type Item struct {
	ID     int    `json:"item_id"`
	SKU    string `json:"sku"`
	Name   string `json:"name"`
	Colour string `json:"colour"`
	Size   string `json:"size"`
	Status int    `json:"status"`
}

// KwitansiBarangMasuk kwitansi penerimaan baranag
type KwitansiBarangMasuk struct {
	ID     int    `json:"kwitansi_id"`
	No     string `json:"no, omitempty"`
	Status int    `json:"status"`
}

// KwitansiDetil detil kwitansi penerimaan barang
type KwitansiDetil struct {
	Kwitansi KwitansiBarangMasuk `json:"kwitansi"`
	Items    []Items             `json:"items"`
}

// Items data item untuk ditampilkan di kwitansi
type Items struct {
	Item             Item               `json:"item"`
	BarangMasuk      BarangMasukOnly    `json:"pemesanan"`
	DetilBarangMasuk []DetilBarangMasuk `json:"detil"`
}

// BarangMasukOnly pemesanan barang untuk di dalam kwitansi
type BarangMasukOnly struct {
	ID            int `json:"barang_masuk_id"`
	JmlhPemesanan int `json:"jmlh_pemesanan"`
	HargaBeli     int `json:"harga_beli"`
	Status        int `json:"status"`
	Total         int `json:"total"`
}

// BarangMasuk barang masuk ke toko
type BarangMasuk struct {
	ID               int                 `json:"barang_masuk_id"`
	JmlhPemesanan    int                 `json:"jmlh_pemesanan"`
	HargaBeli        int                 `json:"harga_beli"`
	Status           int                 `json:"status"`
	Total            int                 `json:"total"`
	Item             Item                `json:"item"`
	Kwitansi         KwitansiBarangMasuk `json:"kwitansi"`
	DetilBarangMasuk []DetilBarangMasuk  `json:"detil"`
}

// DetilBarangMasuk detil dari barang yang masuk
type DetilBarangMasuk struct {
	ID           int    `json:"detil_id"`
	Date         string `json:"date"`
	JmlhDiterima int    `json:"jmlh_diterima"`
}

// BarangKeluar barang yang dijual toko
type BarangKeluar struct {
	ID                int                 `json:"barang_keluar_id"`
	OrderID           string              `json:"order_id"`
	DateTime          string              `json:"date_time"`
	Notes             string              `json:"notes"`
	DetilBarangKeluar []DetilBarangKeluar `json:"detil"`
}

// DetilBarangKeluar detil dari barang yang keluar dari toko
type DetilBarangKeluar struct {
	Item       Item `json:"item"`
	JmlhKeluar int  `json:"jmlh_keluar"`
	HargaJual  int  `json:"harga_jual"`
}

// LNB Laporan Nilai Barang
type LNB struct {
	TanggalCetak      string
	JumlahSKU         int
	JumlahTotalBarang int
	TotalNilai        float64
	Items             []RowNilaiBarang
}

// RowNilaiBarang list laporan nilai barang data
type RowNilaiBarang struct {
	Sku             string
	Name            string
	Size            string
	Colour          string
	BarangMasuk     int
	BarangKeluar    int
	AverageBuyPrice float64
	Stock           int
	Total           float64
}

// LP laporan penjualan
type LP struct {
	TanggalCetak   string
	Tanggal        string
	TotalOmzet     int
	TotalLabaKotor int
	TotalPenjualan int
	TotalBarang    int
	Items          []RowPenjualan
}

// RowPenjualan list data laporan penjualan
type RowPenjualan struct {
	OrderID   string
	Date      string
	SKU       string
	Name      string
	Size      string
	Colour    string
	Jumlah    int
	HargaJual int
	Total     int
	HargaBeli int
	Laba      int
}
