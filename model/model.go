package model

import (
	"database/sql"

	"bitbucket.org/toko/logger"

	_ "github.com/mattn/go-sqlite3"
)

var (
	Db *sql.DB
)

func init() {
	var err error
	Db, err = sql.Open("sqlite3", "./database.db")
	logger.Log(err, logger.FATAL)

	_, err = Db.Exec(
		`PRAGMA foreign_keys = off;
	BEGIN TRANSACTION;
	
	-- Table: barang_keluar
	CREATE TABLE IF NOT EXISTS barang_keluar (barang_keluar_id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, order_id VARCHAR (30) UNIQUE, date_time DATETIME, notes VARCHAR (225));
	
	-- Table: barang_masuk
	CREATE TABLE IF NOT EXISTS barang_masuk (barang_masuk_id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, kwitansi_id INTEGER REFERENCES kwitansi_barang_masuk (kwitansi_id), item_id INTEGER REFERENCES item (item_id) ON DELETE NO ACTION ON UPDATE NO ACTION, jmlh_pemesanan INTEGER, harga_beli INTEGER, status INTEGER (1) DEFAULT (1));
	
	-- Table: detil_barang_keluar
	CREATE TABLE IF NOT EXISTS detil_barang_keluar (item_id INTEGER REFERENCES item (item_id), barang_keluar_id VARCHAR (25) REFERENCES barang_keluar (barang_keluar_id), jmlh_keluar INTEGER, harga_jual INTEGER);
	
	-- Table: detil_barang_masuk
	CREATE TABLE IF NOT EXISTS detil_barang_masuk (detil_id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, barang_masuk_id INTEGER REFERENCES barang_masuk (barang_masuk_id) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL, jmlh_diterima INTEGER, date DATE);
	
	-- Table: item
	CREATE TABLE IF NOT EXISTS item (item_id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL, sku VARCHAR (25) UNIQUE NOT NULL, name VARCHAR (255) NOT NULL, colour VARCHAR (50), size VARCHAR (10), status INTEGER (1) DEFAULT (1));
	
	-- Table: kwitansi_barang_masuk
	CREATE TABLE IF NOT EXISTS kwitansi_barang_masuk (kwitansi_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, "no" VARCHAR (25) UNIQUE, status INTEGER (1) DEFAULT (1));
	
	COMMIT TRANSACTION;
	PRAGMA foreign_keys = on;`)
	logger.Log(err, logger.FATAL)

}
