package model

import (
	"database/sql"
	"fmt"

	"bitbucket.org/toko/logger"

	_ "github.com/mattn/go-sqlite3"
)

func GetAllBarangKeluar() (barang []BarangKeluar, err error) {
	rows, err := Db.Query(`
		SELECT 
			barang_keluar_id,
			date_time,
			notes,
			order_id
		FROM barang_keluar
		ORDER BY date_time DESC
	`)
	defer rows.Close()

	if err != nil {
		logger.Log(err, logger.ERROR)
		return
	}

	for rows.Next() {

		var b BarangKeluar

		err = rows.Scan(&b.ID, &b.DateTime, &b.Notes, &b.OrderID)
		logger.Log(err, logger.ERROR)

		detil, err := getDetilBarangKeluar(b.ID)
		logger.Log(err, logger.ERROR)

		b.DetilBarangKeluar = detil

		barang = append(barang, b)
	}

	return
}

func getDetilBarangKeluar(barangKeluarID int) (barang []DetilBarangKeluar, err error) {

	rows, err := Db.Query(`
		SELECT 
			item.item_id,
			item.sku,
			item.name,
			item.colour,
			item.size,
			item.status,
			detil_barang_keluar.jmlh_keluar,
			detil_barang_keluar.harga_jual
		FROM detil_barang_keluar
		LEFT JOIN item ON item.item_id = detil_barang_keluar.item_id
		WHERE barang_keluar_id = ?
	`, barangKeluarID)
	defer rows.Close()

	if err != nil {
		logger.Log(err, logger.ERROR)
		return
	}

	for rows.Next() {
		var b DetilBarangKeluar
		err = rows.Scan(
			&b.Item.ID,
			&b.Item.SKU,
			&b.Item.Name,
			&b.Item.Colour,
			&b.Item.Size,
			&b.Item.Status,
			&b.JmlhKeluar,
			&b.HargaJual,
		)
		logger.Log(err, logger.ERROR)

		barang = append(barang, b)
	}

	return
}

func AddBarangKeluar(barang BarangKeluar) (barangKeluarID int, err error) {

	stmt, err := Db.Prepare("INSERT INTO barang_keluar(date_time, order_id, notes) values(?,?,?)")
	if err != nil {
		logger.Log(err, logger.ERROR)
		return
	}

	res, err := stmt.Exec(barang.DateTime, barang.OrderID, barang.Notes)
	if err != nil {
		logger.Log(err, logger.ERROR)
		return
	}

	id, err := res.LastInsertId()
	if err != nil {
		logger.Log(err, logger.ERROR)
		return
	}

	barangKeluarID = int(id)

	for _, detil := range barang.DetilBarangKeluar {

		_, err := GetItem(detil.Item.ID)

		if err != nil {
			logger.Log(err, logger.ERROR)

			if err == sql.ErrNoRows {
				err = fmt.Errorf("Item of id %d is not found", detil.Item.ID)
			}

			return 0, err
		}

		stmt, err := Db.Prepare("INSERT INTO detil_barang_keluar(item_id, barang_keluar_id, jmlh_keluar, harga_jual) values(?,?,?,?)")
		logger.Log(err, logger.ERROR)

		_, err = stmt.Exec(detil.Item.ID, barangKeluarID, detil.JmlhKeluar, detil.HargaJual)
		logger.Log(err, logger.ERROR)
	}

	return
}

func EditBarangKeluar(barangKeluarID int, barang BarangKeluar) (err error) {
	stmt, err := Db.Prepare("UPDATE barang_keluar SET date_time = ?, notes = ? WHERE barang_keluar_id = ?")
	logger.Log(err, logger.ERROR)

	_, err = stmt.Exec(barang.DateTime, barang.Notes, barangKeluarID)
	logger.Log(err, logger.ERROR)

	stmt, err = Db.Prepare("DELETE FROM detil_barang_keluar WHERE barang_keluar_id = ?")
	logger.Log(err, logger.ERROR)

	_, err = stmt.Exec(barangKeluarID)
	logger.Log(err, logger.ERROR)

	for _, detil := range barang.DetilBarangKeluar {

		_, err := GetItem(detil.Item.ID)

		if err != nil {
			logger.Log(err, logger.ERROR)

			if err == sql.ErrNoRows {
				err = fmt.Errorf("Item of id %d is not found", detil.Item.ID)
			}

			return err
		}

		stmt, err := Db.Prepare("INSERT INTO detil_barang_keluar(item_id, barang_keluar_id, jmlh_keluar, harga_jual) values(?,?,?,?)")
		logger.Log(err, logger.ERROR)

		_, err = stmt.Exec(detil.Item.ID, barangKeluarID, detil.JmlhKeluar, detil.HargaJual)
		logger.Log(err, logger.ERROR)
	}

	return
}
