package model

import (
	"database/sql"
	"fmt"

	"bitbucket.org/toko/logger"

	_ "github.com/mattn/go-sqlite3"
)

func AddBarangMasuk(barang BarangMasuk) (barangMasukID int, err error) {

	item, err := GetItem(barang.Item.ID)
	logger.Log(err, logger.ERROR)

	if item != (Item{}) {

		kwitansi, err := GetKwitansi(barang.Kwitansi.ID)
		logger.Log(err, logger.ERROR)

		if kwitansi != (KwitansiBarangMasuk{}) {
			stmt, err := Db.Prepare("INSERT INTO barang_masuk(kwitansi_id, item_id, jmlh_pemesanan, harga_beli, status) values(?,?,?,?,1)")
			logger.Log(err, logger.ERROR)

			res, err := stmt.Exec(barang.Kwitansi.ID, barang.Item.ID, barang.JmlhPemesanan, barang.HargaBeli)
			logger.Log(err, logger.ERROR)

			id, err := res.LastInsertId()
			logger.Log(err, logger.ERROR)

			barangMasukID = int(id)
		} else {
			err = fmt.Errorf("Kwitansi of id %d is not found", barang.Kwitansi.ID)
		}
	} else {
		err = fmt.Errorf("Item of id %d is not found", barang.Item.ID)
	}

	return
}

func EditBarangMasuk(barangMasukID int, barang BarangMasuk) (err error) {

	savedB, err := GetBarangMasuk(barangMasukID)

	if err != nil {
		logger.Log(err, logger.ERROR)
		return
	}

	if savedB.Item.ID != barang.Item.ID {
		_, e := GetItem(barang.Item.ID)
		err = e

		if err == sql.ErrNoRows {
			err = fmt.Errorf("Item of id %d is not found", barang.Item.ID)
			return
		}

		if err != nil && err != sql.ErrNoRows {
			logger.Log(err, logger.ERROR)
			return
		}
	}

	if savedB.Kwitansi.ID != barang.Kwitansi.ID {
		_, e := GetKwitansi(barang.Kwitansi.ID)
		err = e

		if err == sql.ErrNoRows {
			err = fmt.Errorf("Kwitansi of id %d is not found", barang.Kwitansi.ID)
			return
		}

		if err != nil && err != sql.ErrNoRows {
			logger.Log(err, logger.ERROR)
			return
		}
	}

	stmt, err := Db.Prepare("UPDATE barang_masuk set kwitansi_id = ?, item_id = ?, jmlh_pemesanan = ?, harga_beli = ?, status = ? WHERE barang_masuk_id = ?")
	logger.Log(err, logger.ERROR)

	_, err = stmt.Exec(barang.Kwitansi.ID, barang.Item.ID, barang.JmlhPemesanan, barang.HargaBeli, barang.Status, barangMasukID)
	logger.Log(err, logger.ERROR)

	return
}

func GetBarangMasuk(barangMasukID int) (barang BarangMasuk, err error) {

	row := Db.QueryRow(`SELECT
		item.item_id,
		item.sku,
		item.name,
		item.colour,
		item.size,
		item.status,
		kwitansi_barang_masuk.kwitansi_id,
		kwitansi_barang_masuk.no,
		kwitansi_barang_masuk.status,
		barang_masuk.barang_masuk_id,
		barang_masuk.jmlh_pemesanan,
		barang_masuk.harga_beli,
		barang_masuk.status,
		(SELECT SUM((barang_masuk.harga_beli * detil_barang_masuk.jmlh_diterima)) as total
		 FROM barang_masuk
		 LEFT JOIN detil_barang_masuk ON barang_masuk.barang_masuk_id = detil_barang_masuk.barang_masuk_id
		 WHERE barang_masuk.barang_masuk_id = ?) AS total
	FROM barang_masuk
	LEFT JOIN item ON item.item_id = barang_masuk.item_id
	LEFT JOIN kwitansi_barang_masuk ON kwitansi_barang_masuk.kwitansi_id = barang_masuk.kwitansi_id
	WHERE barang_masuk.barang_masuk_id = ? AND
	barang_masuk.status = 1`, barangMasukID, barangMasukID)

	err = row.Scan(
		&barang.Item.ID,
		&barang.Item.SKU,
		&barang.Item.Name,
		&barang.Item.Colour,
		&barang.Item.Size,
		&barang.Item.Status,
		&barang.Kwitansi.ID,
		&barang.Kwitansi.No,
		&barang.Kwitansi.Status,
		&barang.ID,
		&barang.JmlhPemesanan,
		&barang.HargaBeli,
		&barang.Status,
		&barang.Total,
	)

	logger.Log(err, logger.ERROR)

	detils, err := GetDetilBarangMasuk(barangMasukID)
	if err == nil && detils != nil {
		barang.DetilBarangMasuk = detils
	}

	return
}

func GetDetilBarangMasuk(barangMasukID int) (barang []DetilBarangMasuk, err error) {

	rows, err := Db.Query("SELECT detil_id, jmlh_diterima, date FROM detil_barang_masuk WHERE barang_masuk_id = ?", barangMasukID)
	defer rows.Close()

	if err != nil {
		logger.Log(err, logger.ERROR)
		return
	}

	for rows.Next() {

		b := DetilBarangMasuk{}

		err = rows.Scan(&b.ID, &b.JmlhDiterima, &b.Date)
		logger.Log(err, logger.ERROR)

		barang = append(barang, b)
	}

	return
}

func AddDetilBarangMasuk(barangMasukID int, detil DetilBarangMasuk) (err error) {

	barang, err := GetBarangMasuk(barangMasukID)
	logger.Log(err, logger.ERROR)

	if barang.ID != 0 {

		stmt, err := Db.Prepare("INSERT INTO detil_barang_masuk(barang_masuk_id, jmlh_diterima, date) values(?,?,?)")
		logger.Log(err, logger.ERROR)

		_, err = stmt.Exec(barangMasukID, detil.JmlhDiterima, detil.Date)
		logger.Log(err, logger.ERROR)
	} else {
		err = fmt.Errorf("Barang masuk of id %d is not found", barangMasukID)
	}

	return
}

func EditDetilBarangMasuk(detilID int, detil DetilBarangMasuk) (err error) {

	stmt, err := Db.Prepare("UPDATE detil_barang_masuk SET jmlh_diterima = ?, date = ? WHERE detil_id = ?")
	logger.Log(err, logger.ERROR)

	_, err = stmt.Exec(detil.JmlhDiterima, detil.Date, detilID)

	return
}

func DeleteDetilBarangMasuk(detilID int) (err error) {
	stmt, err := Db.Prepare("DELETE FROM detil_barang_masuk WHERE detil_id = ?")
	logger.Log(err, logger.ERROR)

	_, err = stmt.Exec(detilID)
	logger.Log(err, logger.ERROR)

	return
}
