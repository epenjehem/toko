package model

import (
	"errors"

	"bitbucket.org/toko/logger"

	_ "github.com/mattn/go-sqlite3"
)

func AddItem(item Item) (err error) {

	i, err := GetItemBySKU(item.SKU)
	logger.Log(err, logger.ERROR)

	if i != (Item{}) {
		err = errors.New("SKU already exist")
		logger.Log(err, logger.INFO)
		return
	}

	stmt, err := Db.Prepare("INSERT INTO item(sku, name, colour, size, status) values(?,?,?,?,1)")
	logger.Log(err, logger.ERROR)

	_, err = stmt.Exec(item.SKU, item.Name, item.Colour, item.Size)
	logger.Log(err, logger.ERROR)

	return
}

func EditItem(itemID int, item Item) (err error) {

	i, err := GetItemBySKU(item.SKU)
	logger.Log(err, logger.ERROR)

	if (i != Item{}) && (i.ID != itemID) {
		err = errors.New("SKU already exist")
		logger.Log(err, logger.INFO)
		return
	}

	stmt, err := Db.Prepare("UPDATE item set sku = ?, name = ?, colour = ?, size = ?, status = ? WHERE item_id = ?")
	logger.Log(err, logger.ERROR)

	_, err = stmt.Exec(item.SKU, item.Name, item.Colour, item.Size, item.Status, itemID)
	logger.Log(err, logger.ERROR)

	return
}

func GetItems() (items []Item, err error) {

	rows, err := Db.Query("SELECT item_id, sku, name, colour, size, status FROM item WHERE status = 1")
	defer rows.Close()

	if err != nil {
		logger.Log(err, logger.ERROR)
		return
	}

	for rows.Next() {

		item := Item{}

		err = rows.Scan(&item.ID, &item.SKU, &item.Name, &item.Colour, &item.Size, &item.Status)
		logger.Log(err, logger.ERROR)

		items = append(items, item)
	}

	return
}

func GetItem(itemID int) (item Item, err error) {

	row := Db.QueryRow("SELECT item_id, sku, name, colour, size, status FROM item WHERE item_id = ? AND status = 1", itemID)
	err = row.Scan(&item.ID, &item.SKU, &item.Name, &item.Colour, &item.Size, &item.Status)

	logger.Log(err, logger.ERROR)

	return
}

func GetItemBySKU(sku string) (item Item, err error) {

	row := Db.QueryRow("SELECT item_id, sku, name, colour, size, status FROM item WHERE sku = ? AND status = 1", sku)
	err = row.Scan(&item.ID, &item.SKU, &item.Name, &item.Colour, &item.Size, &item.Status)

	logger.Log(err, logger.ERROR)

	return
}

func DeleteItem(itemID int) (err error) {
	stmt, err := Db.Prepare("UPDATE item set status = 2 WHERE item_id = ?")
	logger.Log(err, logger.ERROR)

	_, err = stmt.Exec(itemID)
	logger.Log(err, logger.ERROR)

	return
}
