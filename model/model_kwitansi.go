package model

import (
	"errors"

	"bitbucket.org/toko/logger"

	_ "github.com/mattn/go-sqlite3"
)

func AddKwitansi(kw KwitansiBarangMasuk) (err error) {

	k, err := getKwitansiByNo(kw.No)
	logger.Log(err, logger.ERROR)

	if k != (KwitansiBarangMasuk{}) {
		err = errors.New("No kwitansi exist already exist")
		logger.Log(err, logger.INFO)
		return
	}

	stmt, err := Db.Prepare("INSERT INTO kwitansi_barang_masuk(no, status) values(?, ?)")
	logger.Log(err, logger.ERROR)

	_, err = stmt.Exec(kw.No, kw.Status)
	logger.Log(err, logger.ERROR)

	return
}

func EditKwitansi(ID int, kw KwitansiBarangMasuk) (err error) {

	k, err := getKwitansiByNo(kw.No)
	logger.Log(err, logger.ERROR)

	if (k != KwitansiBarangMasuk{}) && (k.ID != ID) {
		err = errors.New("Nomor kwitansi already exist")
		logger.Log(err, logger.INFO)
		return
	}

	stmt, err := Db.Prepare("UPDATE kwitansi_barang_masuk set no = ?, status = ? WHERE kwitansi_id = ?")
	logger.Log(err, logger.ERROR)

	_, err = stmt.Exec(kw.No, kw.Status, ID)
	logger.Log(err, logger.ERROR)

	return
}

func GetAllKwitansi() (kw []KwitansiBarangMasuk, err error) {

	rows, err := Db.Query("SELECT kwitansi_id, no, status FROM kwitansi_barang_masuk WHERE status = 1")
	defer rows.Close()

	if err != nil {
		logger.Log(err, logger.ERROR)
		return
	}

	for rows.Next() {

		kwitansi := KwitansiBarangMasuk{}

		err = rows.Scan(&kwitansi.ID, &kwitansi.No, &kwitansi.Status)
		logger.Log(err, logger.ERROR)

		kw = append(kw, kwitansi)
	}

	return
}

func GetKwitansi(ID int) (kw KwitansiBarangMasuk, err error) {

	row := Db.QueryRow("SELECT kwitansi_id, no, status FROM kwitansi_barang_masuk WHERE kwitansi_id =  ? AND status = 1", ID)
	err = row.Scan(&kw.ID, &kw.No, &kw.Status)

	logger.Log(err, logger.ERROR)

	return
}

func getKwitansiByNo(no string) (kw KwitansiBarangMasuk, err error) {

	row := Db.QueryRow("SELECT kwitansi_id, no, status FROM kwitansi_barang_masuk WHERE no = ? AND status = 1", no)
	err = row.Scan(&kw.ID, &kw.No, &kw.Status)

	logger.Log(err, logger.ERROR)

	return
}

func GetKwitansiDetail(no string) (kw KwitansiDetil, err error) {

	kwitansi, err := getKwitansiByNo(no)
	if err != nil {
		logger.Log(err, logger.ERROR)
		return
	}

	if kwitansi != (KwitansiBarangMasuk{}) {
		kw.Kwitansi = kwitansi

		rows, err := Db.Query(`
			SELECT 
				item.item_id,
				item.sku,
				item.name,
				item.colour,
				item.size,
				item.status,
				masuk.barang_masuk_id,
				masuk.jmlh_pemesanan,
				masuk.harga_beli,
				masuk.status,
				masuk.harga_beli * masuk.jmlh_pemesanan as total
			FROM kwitansi_barang_masuk AS k 
			LEFT JOIN barang_masuk as masuk ON k.kwitansi_id = masuk.kwitansi_id
			LEFT JOIN item ON item.item_id = masuk.item_id
			WHERE masuk.status = 1 AND k.status = 1 AND k."no" = ?`, no)
		defer rows.Close()

		if err != nil {
			logger.Log(err, logger.ERROR)
		}

		for rows.Next() {

			var items Items

			err = rows.Scan(
				&items.Item.ID, &items.Item.SKU, &items.Item.Name, &items.Item.Colour, &items.Item.Size, &items.Item.Status,
				&items.BarangMasuk.ID, &items.BarangMasuk.JmlhPemesanan, &items.BarangMasuk.HargaBeli, &items.BarangMasuk.Status,
				&items.BarangMasuk.Total,
			)
			logger.Log(err, logger.ERROR)

			barang, err := GetDetilBarangMasuk(items.BarangMasuk.ID)
			logger.Log(err, logger.ERROR)

			for _, b := range barang {
				items.DetilBarangMasuk = append(items.DetilBarangMasuk, b)
			}

			kw.Items = append(kw.Items, items)
		}
	}

	return
}

func DeleteKwitansi(ID int) (err error) {

	stmt, err := Db.Prepare("UPDATE kwitansi_barang_masuk set status = 2 WHERE kwitansi_id = ?")
	logger.Log(err, logger.ERROR)

	_, err = stmt.Exec(ID)
	logger.Log(err, logger.ERROR)

	return
}
