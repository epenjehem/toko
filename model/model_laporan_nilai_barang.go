package model

import (
	"fmt"
	"time"

	"bitbucket.org/toko/logger"
	_ "github.com/mattn/go-sqlite3"
)

func LaporanNilaiBarang() (LNB, error) {

	var report LNB

	currentTime := time.Now().Local()
	report.TanggalCetak = currentTime.Format("02 January 2006")

	rows, err := Db.Query(`
		SELECT item.sku,
		item.NAME,
		item.size,
		item.colour,
		(SELECT ifnull(Sum(detil_barang_masuk.jmlh_diterima), 0)
		 FROM   detil_barang_masuk
				LEFT JOIN barang_masuk
					   ON barang_masuk.barang_masuk_id =
						  detil_barang_masuk.barang_masuk_id
		 WHERE  barang_masuk.item_id = item.item_id)        AS barang_masuk,
		(SELECT ifnull(Sum(detil_barang_keluar.jmlh_keluar), 0)
		 FROM   detil_barang_keluar
		 WHERE  detil_barang_keluar.item_id = item.item_id) AS barang_keluar,
		(SELECT ifnull(Avg(barang_masuk.harga_beli), 0)
		 FROM   barang_masuk
		 WHERE  barang_masuk.item_id = item.item_id)        AS average_buy_price
 		FROM   item  WHERE item.status = 1
	`)
	defer rows.Close()

	if err != nil {
		logger.Log(err, logger.ERROR)
	}

	for rows.Next() {

		var i RowNilaiBarang

		err = rows.Scan(
			&i.Sku,
			&i.Name,
			&i.Size,
			&i.Colour,
			&i.BarangMasuk,
			&i.BarangKeluar,
			&i.AverageBuyPrice,
		)

		if err != nil {
			logger.Log(err, logger.ERROR)
		}

		i.Stock = i.BarangMasuk - i.BarangKeluar
		i.Total = i.AverageBuyPrice * float64(i.Stock)

		report.JumlahTotalBarang += i.Stock
		report.TotalNilai += i.Total

		report.Items = append(report.Items, i)
	}

	report.JumlahSKU = len(report.Items)

	return report, err
}

func LaporanPenjualan(startDate, endDate string) (LP, error) {

	// Laba kotor adalah total laba yang memiliki id pemesanan

	var report LP

	currentTime := time.Now().Local()
	report.TanggalCetak = currentTime.Format("02 January 2006")
	report.Tanggal = fmt.Sprintf("%s - %s", startDate, endDate)

	rows, err := Db.Query(`
		SELECT 
			bk.order_id, 
			bk.date_time, 
			item.sku, 
			item.name, 
			item.size, 
			item.colour, 
			ifnull(dbk.jmlh_keluar, 0) as jmlh_keluar,
			ifnull(dbk.harga_jual, 0) as harga_jual,
			( (SELECT ifnull(SUM(barang_masuk.harga_beli), 0) FROM barang_masuk WHERE barang_masuk.item_id = item.item_id)  ) as harga_beli
		FROM item 
		LEFT JOIN detil_barang_keluar dbk ON dbk.item_id = item.item_id
		LEFT JOIN barang_keluar bk ON  bk.barang_keluar_id = dbk.barang_keluar_id	
		WHERE item.status = 1 AND bk.date_time BETWEEN datetime(?) AND datetime(?)
	`, startDate, endDate)

	defer rows.Close()

	if err != nil {
		logger.Log(err, logger.ERROR)
	}

	for rows.Next() {

		var i RowPenjualan

		err = rows.Scan(
			&i.OrderID,
			&i.Date,
			&i.SKU,
			&i.Name,
			&i.Size,
			&i.Colour,
			&i.Jumlah,
			&i.HargaJual,
			&i.HargaBeli,
		)

		if err != nil {
			logger.Log(err, logger.ERROR)
		}

		i.Total = i.Jumlah * i.HargaJual
		i.Laba = i.Total - (i.Jumlah * i.HargaBeli)

		report.TotalBarang += i.Jumlah

		if i.OrderID != "" {
			report.TotalPenjualan++
			report.TotalLabaKotor += i.Laba
		}

		report.TotalOmzet += i.Total

		report.Items = append(report.Items, i)
	}

	return report, err
}
